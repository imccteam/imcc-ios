//
//  SeverityChartCell.swift
//  IMCC
//
//  Created by FPL  on 03/01/17.
//  Copyright © 2017 FPL . All rights reserved.
//

import UIKit

class SeverityChartCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var barChartContainerView: UIView!
    @IBOutlet weak var fromDPBtn: UIButton!
    @IBOutlet weak var toDPBtn: UIButton!
    @IBOutlet weak var barChart: BarChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
