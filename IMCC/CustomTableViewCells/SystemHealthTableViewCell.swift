//
//  SystemHealthTableViewCell.swift
//  IMCC
//
//  Created by CTS on 23/12/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class SystemHealthTableViewCell: UITableViewCell {
    
    @IBOutlet weak var HealthTitleLabel: UILabel!
    @IBOutlet weak var accessoryButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
