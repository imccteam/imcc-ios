//
//  DetailSystemHealthTitleTableViewCell.swift
//  IMCC
//
//  Created by CTS on 27/12/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class DetailSystemHealthTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var detailTitleLabel: UILabel!
    
    @IBOutlet weak var detailCurrentStatus: UILabel!
    
    @IBOutlet weak var detailPostStatus: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
