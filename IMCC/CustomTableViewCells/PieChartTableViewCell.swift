//
//  PieChartTableViewCell.swift
//  IMCC
//
//  Created by FPL  on 21/12/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class PieChartTableViewCell: UITableViewCell {

    @IBOutlet weak var fromDPBtn: UIButton!
    @IBOutlet weak var toDPBtn: UIButton!
    @IBOutlet weak var assigneeBtn: UIButton!

    @IBOutlet weak var pieChartContainerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
