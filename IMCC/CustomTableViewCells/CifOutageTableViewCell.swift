//
//  CifOutageTableViewCell.swift
//  IMCC
//
//  Created by CTS on 10/01/17.
//  Copyright © 2017 FPL . All rights reserved.
//

import UIKit


class CifOutageTableViewCell: UITableViewCell, UUChartDataSource{

    @IBOutlet weak var countyContainerView: UIView!
    
    @IBOutlet weak var cifContainerView: UIView!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
   
    var chartView:UUChart!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: Segment Control
    @IBAction func segmentControlAction(_ sender: Any) {
        if(segmentControl.selectedSegmentIndex == 0)
        {
           cifContainerView.isHidden = true
           countyContainerView.isHidden = false
        }
        else
        {
           countyContainerView.isHidden = true
          cifContainerView.isHidden = false
        }
    }
    
    //MARK: CUSTOM METHODS
    
    func configUI(width:(CGFloat), frameHeight:(CGFloat)) -> Void {
        let frameWidth:CGFloat
        
        if((chartView) != nil)
        {
            chartView.removeFromSuperview()
            chartView = nil
        }
        
        if(UI_USER_INTERFACE_IDIOM() == .pad)
        {
            frameWidth = width - 110
        }
        else
        {
            frameWidth = width - 70
        }
        
        chartView = UUChart.init(frame: CGRect(x: 10, y: 25, width: frameWidth, height: countyContainerView.frame.size.height - 30), dataSource: self, style: .bar)
        
        chartView.backgroundColor = UIColor.clear
        
        chartView.show(in: countyContainerView)
  
    }
    
    func getXTitles(num:(Int)) -> NSArray {
        let xTitle:NSArray = ["Broward","Palm Beach","Miami-Dade"]
        return xTitle
    }
    
    
    //MARK: UUCHARTDATA SOURCE 
    //MARK:REQUIRED METHODS
    
    func chartConfigAxisXLabel(_ chart: UUChart!) -> [Any]! {
        let configXLabel:[Any] = getXTitles(num: 3) as! [Any]
       return configXLabel
    }
    
    func chartConfigAxisYValue(_ chart: UUChart!) -> [Any]! {
        let firstArray:NSArray = ["10000","15000","25000"];
        let secondArray:NSArray = ["5000","12000","8000"];
        let thirdArray:NSArray = ["0","7000","2000"];
        
        let arraySet:[Any] = [firstArray,secondArray,thirdArray] 
        
        return arraySet
    }
    
    //MARK:OPTIONAL METHODS
    
    func chartConfigColors(_ chart: UUChart!) -> [Any]! {
        let colorArraySet:[Any] = [UIColor(hexString:"5FB0E7")
                                  ,UIColor(hexString:"C3D600")
                                  ,UIColor(hexString:"68BB45")]
        
        return colorArraySet
    }
    
    func chartRange(_ chart: UUChart!) -> CGRange {
        return CGRange(max: 28000, min: 0)
    }
    
    func chartHighlightRange(inLine chart: UUChart!) -> CGRange {
        return CGRangeZero
    }
    
    func chart(_ chart: UUChart!, showHorizonLineAt index: Int) -> Bool {
        return true
    }
    
    func chart(_ chart: UUChart!, showMaxMinAt index: Int) -> Bool {
        return true
    }

}
