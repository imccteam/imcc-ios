//
//  DetailSystemHealthTableViewCell.swift
//  IMCC
//
//  Created by CTS on 26/12/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class DetailSystemHealthTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var currentStatusView: UIView!
   
    @IBOutlet weak var postStatusView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
