//
//  EventOutageTableViewCell.swift
//  IMCC
//
//  Created by CTS on 10/01/17.
//  Copyright © 2017 FPL . All rights reserved.
//

import UIKit

class EventOutageTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var eventBarChart: BarChartView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
