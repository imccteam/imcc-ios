//
//  AssigneeTableViewCell.swift
//  IMCC
//
//  Created by FPL  on 09/01/17.
//  Copyright © 2017 FPL . All rights reserved.
//

import UIKit

class AssigneeTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnExpanded: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.setCornerRadiusWithBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
