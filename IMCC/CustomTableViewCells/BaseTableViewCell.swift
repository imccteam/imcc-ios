//
//  BaseTableViewCell.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFirstImg: UILabel!
    @IBOutlet weak var lblCheck: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
