//
//  EmployeeListTableViewCell.swift
//  IMCC
//
//  Created by CTS on 28/12/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class EmployeeListTableViewCell: UITableViewCell {

    @IBOutlet weak var employeeName: UILabel!
    
  
    @IBOutlet weak var employeeStatusView: UIButton!
    
    @IBOutlet weak var upperSeperatorView: UIView!
    
    @IBOutlet weak var expandListButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
