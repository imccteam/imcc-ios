//
//  UIView.swift
//  IMCC
//
//  Created by mnameit on 14/12/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

// MARK: set border for the uiview
extension UIView{
    
    func setBorder(toView: UIView) -> Void {
        let viewLayer = toView.layer
        viewLayer.borderWidth = 2
        viewLayer.borderColor = UIColor(red:196/255.0, green:200/255.0, blue:199/255.0, alpha: 1.0).cgColor
    }

    func setCornerRadiusWithBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red:196/255.0, green:200/255.0, blue:199/255.0, alpha: 1.0).cgColor
        self.layer.cornerRadius = 2
    }

    func setShadow() {
        self.layer.shadowColor = UIColor(red:129/255.0, green:139/255.0, blue:141/255.0, alpha: 1.0).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = false
    }

}

