//
//  IMCCDashboardViewController.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class IMCCDashboardViewController: UIViewController {
    
    // Properties
    @IBOutlet weak var segmentFirst: UISegmentedControl!
    @IBOutlet weak var segmentSecond: UISegmentedControl!
    @IBOutlet weak var tblView: UITableView!
    
    weak var delegate: LeftMenuProtocol!
    
    // MARK: View Controller Life Cycle
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewDidLoad
    
    override func viewDidLoad() {
        // Call Super
        super.viewDidLoad()
        
        // register Notifiiocations
        NotificationCenter.default.addObserver(self, selector: #selector(actionOnLegends(_:)), name: NSNotification.Name(rawValue: "LegendAction"), object: nil)
        
        // Do any additional setup after loading the view.
        self.setNavigationBarItem(titleString: "IMCC Dashboard")
    }
    
    // MARK: Action Method
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // actionOnButton:
    
    @IBAction func actionOnButton(_ sender: Any) {
        // Navigation
        if (delegate != nil) {
            delegate.changeViewController(LeftMenu(rawValue: 3)!)
        }
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // actionOnButton:
    
    @IBAction func actionOnLegends(_ sender: Any) {
        // Navigation
        if (delegate != nil) {
            delegate.changeViewController(LeftMenu(rawValue:7)!)
        }
    }
}


// MARK:Table View Delegate Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCDashboardViewController : UITableViewDelegate {
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            // device is ipad
            return 456
        }
        else{
            //  device is iPhone
            return 316
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

// MARK:Table View Data Source Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCDashboardViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let pieCell = tableView.dequeueReusableCell(withIdentifier: "PieCell", for: indexPath) as! PieChartTableViewCell
            
            pieCell.selectionStyle = UITableViewCellSelectionStyle.none
            
            // Draw the Pie chart
            let pieChart = PCPieChart(frame: CGRect(x: 0, y: 0, width: pieCell.pieChartContainerView.frame.size.width, height: pieCell.pieChartContainerView.frame.size.height))
        
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
                pieChart.diameter = 280
            }
            else{
               pieChart.diameter = 160
            }
            pieChart.sameColorLabel = true
            
            // Add Components
            let components:NSMutableArray = NSMutableArray()
            
            for index in 1...5 {
                if index == 1 {
                    let component = PCPieComponent.pieComponent(withTitle: "", value: 20) as! PCPieComponent
                    components.add(component)
                    component.colour = UIColor(hexString: "C3D600")
                }
                if index == 2 {
                    let component = PCPieComponent.pieComponent(withTitle: "", value: 30) as! PCPieComponent
                    components.add(component)
                    component.colour = UIColor(hexString: "68BB45")
                }
                if index == 3 {
                    let component = PCPieComponent.pieComponent(withTitle: "", value: 15) as! PCPieComponent
                    components.add(component)
                    component.colour = UIColor(hexString: "0C2739")
                }
                if index == 4 {
                    let component = PCPieComponent.pieComponent(withTitle: "", value: 20) as! PCPieComponent
                    components.add(component)
                    component.colour = UIColor(hexString: "5FB0E7")
                }
                if index == 5 {
                    let component = PCPieComponent.pieComponent(withTitle: "", value: 15) as! PCPieComponent
                    components.add(component)
                    component.colour = UIColor(hexString: "0096DB")
                }
            }
            // Add components to chart and chart to container view
            pieChart.components = components
            pieCell.pieChartContainerView.addSubview(pieChart)

        
          // Add Target
        pieCell.assigneeBtn.addTarget(self, action: #selector(self.actionOnLegends(_:)), for: .touchUpInside)
        
        // Return Cell
            pieCell.selectionStyle = .none
            return pieCell
      }
       else{
            let barCell = tableView.dequeueReusableCell(withIdentifier: "BarCell", for: indexPath) as! SeverityChartCell
        
            if(barCell.barChart.tag == 1)
            {
            barCell.selectionStyle = UITableViewCellSelectionStyle.none
            barCell.barChart.legendColorCodeArray = ["68BB45", "84C86A", "A2D58F", "C1E3B5", "E0F1DA"];

            
            // store the Chart Data in an array
            let array:NSArray =  barCell.barChart.createChartData(withTitles: (NSArray(objects: "Sev1", "Sev2", "Sev3", "Sev4","Sev5") as NSArray!) as! [Any]
                , values: (NSArray(objects: "50", "40", "22", "5", "5") as NSArray!) as! [Any], colors: (NSArray(objects: "68BB45", "84C86A", "A2D58F", "C1E3B5", "E0F1DA") as NSArray!) as! [Any]
                , labelColors: (NSArray(objects: "000000", "000000", "000000", "000000","000000")) as! [Any]) as NSArray
            
            
            //feature of the bar
            barCell.barChart.flag = 1
            barCell.barChart.interval = 10
            barCell.barChart.setupBarViewShape(BarShapeSquared)
            barCell.barChart.setupBarViewStyle(BarStyleFlat)
            barCell.barChart.setupBarViewShadow(BarShadowNone)
            
            //passing the chart Data
            
            barCell.barChart.setDataWith(array as! [Any], showAxis: DisplayOnlyYAxis, with: UIColor.darkGray, shouldPlotVerticalLines:true)
                barCell.barChart.tag = 0
            }
            // Return Cell
           return barCell
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.reloadData()
    }
}
