//
//  IMCCLeftSliderViewController.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case home = 0
    case imcc
    case outage
    case system
    case status
    case about
    case healthDetail
    case assigneeTickets
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class IMCCLeftSliderViewController: UIViewController, LeftMenuProtocol {
    
    // Properties
    @IBOutlet weak var tableView: UITableView!
    
    // Menu Options
    var menus = ["Home", "IMCC Dashboard", "Outage Dashboard", "System Health", "Employee Status", "About"]
    
    // Controllers
    var homeViewController: UIViewController!
    var imccDashboardViewController: UIViewController!
    var outageViewController: UIViewController!
    var employeeStatusViewController: UIViewController!
    var systemHealthViewController: UIViewController!
    var aboutViewController: UIViewController!
    var detailSystemHealthController: UIViewController!
    var assigneeTicketsController: UIViewController!

    // MARK: Custom Method
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // changeViewController:
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .home:
            self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
        case .imcc:
            self.slideMenuController()?.changeMainViewController(self.imccDashboardViewController, close: true)
        case .outage:
            self.slideMenuController()?.changeMainViewController(self.outageViewController, close: true)
        case .system:
            self.slideMenuController()?.changeMainViewController(self.systemHealthViewController, close: true)
        case .status:
            self.slideMenuController()?.changeMainViewController(self.employeeStatusViewController, close: true)
        case .about:
            self.slideMenuController()?.changeMainViewController(self.aboutViewController, close: true)
        case .healthDetail:
            self.slideMenuController()?.changeMainViewController(self.detailSystemHealthController, close: true)
        case .assigneeTickets:
            self.slideMenuController()?.changeMainViewController(self.assigneeTicketsController, close: true)
        }
    }
    
    // MARK: View Controller Cycle
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // init:
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewDidLoad
    
    override func viewDidLoad() {
        // Call Super
        super.viewDidLoad()
        
        // Table View Seprator
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        
        // Set Left Menu Controller Options
        var storyboard:UIStoryboard!
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            // device is ipad
            storyboard = UIStoryboard(name: "Storyboard_iPad", bundle: nil)
        }
        else{
            //  device is iPhone
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }
        let imccDashboardViewController = storyboard.instantiateViewController(withIdentifier: "IMCCDashboardCon") as! IMCCDashboardViewController
        self.imccDashboardViewController = UINavigationController(rootViewController: imccDashboardViewController)
        imccDashboardViewController.delegate = self
        
        let outageViewController = storyboard.instantiateViewController(withIdentifier: "IMCCOutageCon") as! IMCCOutageDashboardViewController
        self.outageViewController = UINavigationController(rootViewController: outageViewController)
        
        let systemHealthViewController = storyboard.instantiateViewController(withIdentifier: "IMCCSystemCon") as! IMCCSystemHealthViewController
        systemHealthViewController.delegate = self
        self.systemHealthViewController = UINavigationController(rootViewController: systemHealthViewController)
        
        let employeeStatusViewController = storyboard.instantiateViewController(withIdentifier: "IMCCStatusCon") as! IMCCEmployeeStatusViewController
        self.employeeStatusViewController = UINavigationController(rootViewController: employeeStatusViewController)
        
        let aboutViewController = storyboard.instantiateViewController(withIdentifier: "IMCCAboutCon") as! IMCCAboutViewController
        self.aboutViewController = UINavigationController(rootViewController: aboutViewController)
        
        let detailSystemHealthController = storyboard.instantiateViewController(withIdentifier: "IMCCHealthDetailCon") as! DetailSystemHealthViewController
        self.detailSystemHealthController = UINavigationController(rootViewController: detailSystemHealthController)
        detailSystemHealthController.delegate = self
        
        let assigneeTicketController = storyboard.instantiateViewController(withIdentifier: "IMCCAssigneeTickets") as! IMCCAssignedTicketViewController
        assigneeTicketController.delegate = self
        self.assigneeTicketsController = UINavigationController(rootViewController: assigneeTicketController)
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewDidAppear
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewDidLayoutSubviews
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
}

// MARK:Table View Delegate Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCLeftSliderViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .home, .imcc, .outage, .system, .status, .about, .healthDetail:
                return 50
                
            default: break
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        cell.backgroundColor = UIColor.clear
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

// MARK:Table View Data Source Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCLeftSliderViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SliderCellIdentifier", for: indexPath) as! BaseTableViewCell
        cell.contentView.backgroundColor = UIColor.clear
        
        let indexName = menus[indexPath.row]
        cell.lblName?.text = indexName
        var imgString:String!
        
        if indexPath.row == 0 {
            // Home
            imgString = "L"
        }
        else if indexPath.row == 1{
            // IMCC Dashboard
            imgString = "D"
        }
        else if indexPath.row == 2{
            // Outage Dashboard
            imgString = "E"
        }
        else if indexPath.row == 3{
            // System Health
            imgString = "I"
        }
        else if indexPath.row == 4{
            // Employee Status
            imgString = "F"
        }
        else{
            // About
            imgString = "H"
        }

        // Set Image
        cell.imgBtn?.setTitle(imgString, for: .normal)
        cell.imgBtn?.setTitleColor(UIColor.white, for: .normal)
        
        cell.selectionStyle = .none
        
        // Return
        return cell
    }
}
