//
//  IMCCSortViewController.swift
//  IMCC
//
//  Created by FPL  on 10/01/17.
//  Copyright © 2017 FPL . All rights reserved.
//

import UIKit

class IMCCSortViewController: UIViewController {
    
    // Properties
    @IBOutlet weak var tblView: UITableView!
    
    // Models
    var selectedRowDict:NSMutableDictionary = NSMutableDictionary()
    
    // MARK: View Controller Life Cycle
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewDidLoad
    
    override func viewDidLoad() {
        // Call Super
        super.viewDidLoad()
        
        // Title
        self.title = "Sort By"
        
        // Set Navigation Bar Color
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = UIColor(red: 95.0/255.0, green: 176.0/255.0, blue: 231.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor(red: 95.0/255.0, green: 176.0/255.0, blue: 231.0/255.0, alpha: 1.0)]
        
        // Set Right button
        let btn = UIButton(type: .custom)
        btn.setTitle("Done", for: .normal)
        btn.setTitleColor(UIColor(red: 95.0/255.0, green: 176.0/255.0, blue: 231.0/255.0, alpha: 1.0), for: .normal)
        btn.titleLabel?.font = UIFont(name: "HelveticaNeueLTStd-Md", size: 18)
        btn.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -35);
        btn.addTarget(self, action: #selector(self.actionOnDone), for: .touchUpInside)
        let rightButton = UIBarButtonItem(customView: btn)
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    // MARK: Action Method
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // actionOnDone
    
    func actionOnDone() {
        self.dismiss(animated: true, completion: {});
    }
}

// MARK:Table View Delegate Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCSortViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = UIColor.clear
        vw.frame = CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: 45)
        let label = UILabel(frame: CGRect(x: 64, y:10, width: 400, height: 25))
        label.font = UIFont(name: "HelveticaNeueLTStd-Md", size: 16)
        label.backgroundColor = UIColor.clear
        
        if section == 0 {
            label.text = "Sort Order"
        }
        else{
            label.text = "Sort By"
        }
        // Add subview
        vw.addSubview(label)
        
        // Add border
        let borderColor = UIColor(red: 196.0/255.0, green: 200.0/255.0, blue: 199.0/255.0, alpha: 1.0)
        vw.layer.addBorder(edge: UIRectEdge.top, color: borderColor, thickness: 1)
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // If selected row is available then remove
        if indexPath.section == 0 {
            selectedRowDict.setValue(indexPath.row, forKey: "0")
        }
        else{
            selectedRowDict.setValue(indexPath.row, forKey: "1")
        }
        
        // Reload table
        self.tblView.reloadData()
    }
}

// MARK:Table View Data Source Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCSortViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sortCell = tableView.dequeueReusableCell(withIdentifier: "SortCell", for: indexPath) as! BaseTableViewCell
        
        let borderColor = UIColor(red: 196.0/255.0, green: 200.0/255.0, blue: 199.0/255.0, alpha: 1.0)
        sortCell.contentView.layer.addBorder(edge: UIRectEdge.top, color: borderColor, thickness: 1.0)
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                sortCell.lblFirstImg.text = "C"
                sortCell.lblName.text = "Sort Ascending"
            }
            else {
                sortCell.lblFirstImg.text = "A"
                sortCell.lblName.text = "Sort Descending"
            }
        }
        else{
            if indexPath.row == 0 {
                sortCell.lblFirstImg.text = "7"
                sortCell.lblName.text = "Create Date"
            }
            else if indexPath.row == 1{
                sortCell.lblFirstImg.text = "6"
                sortCell.lblName.text = "SLA Due Date"
            }
            else {
                sortCell.lblFirstImg.text = "2"
                sortCell.lblName.text = "Status"
                
                sortCell.contentView.layer.addBorder(edge: UIRectEdge.bottom, color: borderColor, thickness: 1.0)
                
            }
        }
        
        // Hide Or Show Tick Mark
        if selectedRowDict["0"] != nil && indexPath.section == 0 {
            let val:Int = selectedRowDict["0"] as! Int
            if val == indexPath.row {
                sortCell.lblCheck.isHidden = false
            }
            else{
                sortCell.lblCheck.isHidden = true
            }
        }
        else if selectedRowDict["1"] != nil && indexPath.section == 1 {
            let val:Int = selectedRowDict["1"] as! Int
            if val == indexPath.row {
                sortCell.lblCheck.isHidden = false
            }
            else{
                sortCell.lblCheck.isHidden = true
            }
        }
        else{
            sortCell.lblCheck.isHidden = true
        }
        
        // Selection style
        sortCell.selectionStyle = .none
        
        return sortCell
    }
    
}
