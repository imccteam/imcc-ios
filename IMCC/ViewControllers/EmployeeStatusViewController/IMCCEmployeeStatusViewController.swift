//
//  IMCCEmployeeStatusViewController.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class IMCCEmployeeStatusViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate {
    
    
    @IBOutlet weak var EmployeeStatusTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()  
        self.setNavigationBarItem(titleString: "Employee Status")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: TABLE VIEW DELEGATE AND DATA SOURCE METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // Supervisor, Direct Reports
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0)
        {
            //Supervisor Section
            return "Supervisor"
        }
        //Direct Reports Section
        return "Direct Reports"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let sectionTitle = UILabel()
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        {
            // device ipad
            sectionTitle.font = UIFont.boldSystemFont(ofSize: 20)
        }
        else
        {
            //device iPhone
            sectionTitle.font = UIFont.boldSystemFont(ofSize: 16)
        }
        
        let headerView =   view as! UITableViewHeaderFooterView
        
        headerView.textLabel?.font = sectionTitle.font
        headerView.textLabel?.textAlignment = .left
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
            //Supervisor Section
            return 2
        }
        
        //Direct Reports Section
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        {
            // device is ipad
            return 70
        }
        //device iPhone
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0 )
        {
            // Supervisor Cell
            let supervisorCell = tableView.dequeueReusableCell(withIdentifier: "EmployeeStatusIdentifier", for: indexPath) as! EmployeeListTableViewCell
            
            if(indexPath.row == 0)
            {
                // hides the upper seperator
                supervisorCell.upperSeperatorView.isHidden = false
            }
            
            supervisorCell.expandListButton.isHidden = true
            supervisorCell.employeeName.text = "Ian Robinson"
            supervisorCell.employeeStatusView.backgroundColor = UIColor(hexString:"68BB45")
            
            return supervisorCell
        }
            
        else
        {
            //Direct Reports Cell
            let directReportCell = tableView.dequeueReusableCell(withIdentifier: "EmployeeStatusIdentifier", for: indexPath) as! EmployeeListTableViewCell
            
            if(indexPath.row == 0)
            {
                // hide the upper seperator
                directReportCell.upperSeperatorView.isHidden = false
            }
            directReportCell.expandListButton.isHidden = false
            directReportCell.employeeName.text = "Amit Grover"
           directReportCell.employeeStatusView.backgroundColor = UIColor(hexString:"C3D600")
            
            return directReportCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        {
            // device is ipad
            return 76
        }
        //device iPhone
        return 60
    }
    
    //MARK: SERACH BAR DELEGATE METHODS
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.setShowsCancelButton(true, animated: true)
        print(searchText)
        //do something
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        EmployeeStatusTableView.reloadData()

    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
}
