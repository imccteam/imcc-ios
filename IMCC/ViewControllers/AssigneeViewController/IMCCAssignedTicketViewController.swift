//
//  IMCCAssignedTicketViewController.swift
//  IMCC
//
//  Created by FPL  on 09/01/17.
//  Copyright © 2017 FPL . All rights reserved.
//

import UIKit

class IMCCAssignedTicketViewController: UIViewController {
    
    weak var delegate:LeftMenuProtocol!

    // Properties
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    // Variables
    var expandedRowsArr:NSMutableArray = NSMutableArray()
    
    // MARK: View Controller Life Cycle
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewDidLoad
    
    override func viewDidLoad() {
        // Call Super
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBarItem(titleString: "Assigned")
        
        // Add shadow
        self.viewHeader.setShadow()
    }
    
    // MARK: Orientation Delegates
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewWillTransitionToSize
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.viewHeader.setShadow()
    }
    
    // MARK: Action Method
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // actionOnHeaderButton:
    @IBAction func actionOnHeaderButton(_ sender: Any) {
        
        // Navigation
        if (delegate != nil) {
            delegate.changeViewController(LeftMenu(rawValue: 1)!)
        }
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // actionOnFilterButton:
    
    @IBAction func actionOnFilterButton(_ sender: Any) {
        // Set Left Menu Controller Options
        var storyboard:UIStoryboard!
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            // device is ipad
            storyboard = UIStoryboard(name: "Storyboard_iPad", bundle: nil)
        }
        else{
            //  device is iPhone
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }
        let imccSortCon = storyboard.instantiateViewController(withIdentifier: "SortController") as! IMCCSortViewController
        let navCon = UINavigationController(rootViewController: imccSortCon)
        self.present(navCon, animated: true, completion: nil)
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // actionOnExpandedButton:
    
    func actionOnExpandedButton(_ sender: Any) {
        let tag = (sender as AnyObject).tag
        let rowClicked = NSNumber(value: tag!)
        
        if expandedRowsArr.contains(rowClicked) {
            expandedRowsArr.remove(rowClicked)
        }
        else{
            expandedRowsArr.add(rowClicked)
        }
        
        // Reload table view
        self.tblView.reloadData()
    }
}


// MARK:Table View Delegate Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCAssignedTicketViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 0.0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row  = indexPath.row
        let rowClicked = NSNumber(value: row)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            if expandedRowsArr.contains(rowClicked) {
                //MARK: calculate for dynamic row height
                return 429.0
            }
            else{
                return 258.0
            }
        }
        else{
            if expandedRowsArr.contains(rowClicked) {
                //MARK: calculate for dynamic row height
                return 242.0
            }
            else{
                return 141.0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

// MARK:Table View Data Source Methods
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCAssignedTicketViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row  = indexPath.row
        let rowClicked = NSNumber(value: row)
        
        if expandedRowsArr.contains(rowClicked) {
            let assigneeMoreCell = tableView.dequeueReusableCell(withIdentifier: "AssigneeMoreCell", for: indexPath) as! AssigneeMoreTableViewCell
            
            // Action On button
            assigneeMoreCell.btnClose.addTarget(self, action: #selector(actionOnExpandedButton(_:)), for: .touchUpInside)
            assigneeMoreCell.btnClose.tag = row
            
            // Selection style
            assigneeMoreCell.selectionStyle = .none
            
            //MARK: Calculate Labels height for description and others then increase the container view height and increase bgview height
            return assigneeMoreCell
        }
        else{
            let assigneeCell = tableView.dequeueReusableCell(withIdentifier: "AssigneeCell", for: indexPath) as! AssigneeTableViewCell
            
            // Selection style
            assigneeCell.selectionStyle = .none
            
            // Action On button
            assigneeCell.btnExpanded.addTarget(self, action: #selector(actionOnExpandedButton(_:)), for: .touchUpInside)
            assigneeCell.btnExpanded.tag = row

            return assigneeCell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 293.5
    }

}
