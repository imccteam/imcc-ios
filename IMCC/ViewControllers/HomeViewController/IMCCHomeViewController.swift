//
//  IMCCHomeViewController.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class IMCCHomeViewController: UIViewController {
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var locBtn: UIButton!
    @IBOutlet weak var calBtn: UIButton!
    @IBOutlet weak var timeBtn: UIButton!
    
    // MARK: View Controller Life Cycle
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // viewDidLoad
    
    override func viewDidLoad() {
        // Call Super
        super.viewDidLoad()
        
        self.setNavigationBarItem(titleString: "IM Command Center")
        
        // Check for orientation and set stack views axis
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            if UIDeviceOrientationIsPortrait(UIDevice.current.orientation){
                self.stackView.axis = .vertical
            }else{
                self.stackView.axis = .horizontal
            }
        }
    }
    
    // MARK: Orientation Delegate Method
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // didRotate
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            switch UIDevice.current.orientation {
            case .portrait:
                self.stackView.axis = .vertical
            case .portraitUpsideDown:
                self.stackView.axis = .vertical
            case .landscapeLeft:
                self.stackView.axis = .horizontal
            case .landscapeRight:
                self.stackView.axis = .horizontal
                //MARK: Improve needed
            default:
                self.stackView.axis = .vertical
            }
            
            // Border
            let borderColor = UIColor(red: 196.0/255.0, green: 200.0/255.0, blue: 199.0/255.0, alpha: 1.0)
            self.lblHeader.layer.addBorder(edge: UIRectEdge.bottom, color: borderColor, thickness: 1.0)
             self.lblHeader.setShadow()
        }
    }
}

// MARK: SlideMenuController Delegate
// -------------------------------------------------------------------------------------------------------------------------------------------
//

extension IMCCHomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
