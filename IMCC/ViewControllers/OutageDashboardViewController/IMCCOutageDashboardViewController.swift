//
//  IMCCOutageDashboardViewController.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class IMCCOutageDashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var outageTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBarItem(titleString: "Outage Dashboard")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        let currentOrientation:UIDeviceOrientation = UIDevice.current.orientation
        
        switch currentOrientation {
        case .landscapeLeft:
            outageTableView.reloadData()
            break
        case .landscapeRight:
            outageTableView.reloadData()
            break
        default:
            outageTableView.reloadData()
            break
        }
    }
    
    //MARK: Table view data source and delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let eventCell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventOutageTableViewCell
            
            if(eventCell.eventBarChart.tag == 1)
            {
            eventCell.selectionStyle = UITableViewCellSelectionStyle.none
            
            let array:NSArray = eventCell.eventBarChart.createChartData(withTitles: (NSArray(objects: "Cust Out", "Cust Resorted", "Cust Affected") as NSArray!) as! [Any]
                , values: (NSArray(objects: "318860,", "1118840", "1482490") as NSArray!) as! [Any], colors: (NSArray(objects: "CEEAF7", "9CD4F0", "6BBFE8") as NSArray!) as! [Any]
                , labelColors: (NSArray(objects: "000000", "000000", "000000")) as! [Any]) as NSArray
            
            //feature of the bar
            eventCell.eventBarChart.flag = 0; //differentiate outage & severity Cell
            eventCell.eventBarChart.interval = 200000
            
            eventCell.eventBarChart.setupBarViewShape(BarShapeSquared)
            eventCell.eventBarChart.setupBarViewStyle(BarStyleFlat)
            eventCell.eventBarChart.setupBarViewShadow(BarShadowNone)
            
            //passing the chart Data
            
            eventCell.eventBarChart.setDataWith(array as! [Any], showAxis: DisplayOnlyYAxis, with: UIColor.darkGray, shouldPlotVerticalLines:false)
            eventCell.eventBarChart.tag = 0
            }
            return eventCell
            
            
        }
        else
        {
         let cifCell = tableView.dequeueReusableCell(withIdentifier: "CifCell", for: indexPath) as! CifOutageTableViewCell
            cifCell.configUI(width: tableView.bounds.size.width, frameHeight: tableView.bounds.size.height)
            return cifCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        {
            return 487
        }
        else
        {
        return 337
        }
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
