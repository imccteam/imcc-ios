//
//  IMCCSystemHealthViewController.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit


class IMCCSystemHealthViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    weak var delegate: LeftMenuProtocol!

    @IBOutlet weak var tableView: UITableView!
    
    
    // sample Data Lisit

    let HealthTitleArray:[String] = ["Customer Facing Systems","Core Back End","Sample 3","Sample 4","Sample 5"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // nav bar extension method
        self.setNavigationBarItem(titleString: "System Health")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func accessoryButtonOnTap() {
        // Navigation
        if (delegate != nil) {
            delegate.changeViewController(LeftMenu(rawValue: 6)!)
        }
    }
    // MARK: Table view Delegate and Datasource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HealthTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HealthCellIdentifier", for: indexPath) as! SystemHealthTableViewCell
        
        cell.HealthTitleLabel.text = HealthTitleArray[indexPath.row]
        
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        cell.backgroundColor = UIColor(red: 241.0/255.0, green: 248.0/255.0, blue: 253.0/255.0, alpha: 1.0)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) ->CGFloat
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            // device is ipad
            return 70
        }
        //device iPhone
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            accessoryButtonOnTap()
    
    }

}
