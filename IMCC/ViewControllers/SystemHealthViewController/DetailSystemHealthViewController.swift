//
//  SystemHealthDetailViewController.swift
//  IMCC
//
//  Created by CTS on 23/12/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

class DetailSystemHealthViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    weak var delegate: LeftMenuProtocol!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var screenTitleLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var screenTitle:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // nav bar display
        self.setNavigationBarItem(titleString: "System Health")
        
        // button target
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        // title for the page
         screenTitleLabel.text = "Customer Facing Systems"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK: Back Navigation Action Method
    
     func backButtonTapped(sender: UIButton) {
        // Navigation
        if (self.delegate != nil) {
            self.delegate.changeViewController(LeftMenu(rawValue: 3)!)
        }
    }
    
    //MARK: Table View Delegate and Data Source Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if(section == 0)
       {
        return 1
        }
        
       else
       {
        return 10
       }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if(indexPath.section == 0)
        {
            let detailTitleCell = tableView.dequeueReusableCell(withIdentifier: "DetailTitleCell", for: indexPath) as! DetailSystemHealthTitleTableViewCell
            
             detailTitleCell.selectionStyle = UITableViewCellSelectionStyle.none
            
            // column heading feature for iPad and iPhone
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
            {
                // cell setting for iPad
                detailTitleCell.detailTitleLabel.font = UIFont.boldSystemFont(ofSize: 22)
                detailTitleCell.detailCurrentStatus.font = UIFont.boldSystemFont(ofSize: 22)
                detailTitleCell.detailPostStatus.font = UIFont.boldSystemFont(ofSize: 22)
            }
            else
            {
                // cell setting for iPhone
                detailTitleCell.detailTitleLabel.font = UIFont.boldSystemFont(ofSize: 18)
                detailTitleCell.detailCurrentStatus.font = UIFont.boldSystemFont(ofSize: 18)
                detailTitleCell.detailPostStatus.font = UIFont.boldSystemFont(ofSize: 18)
            }
            
           detailTitleCell.detailTitleLabel.text = "System"
           detailTitleCell.detailCurrentStatus.text = "Current"
           detailTitleCell.detailPostStatus.text = "Post Storm"
            
            // Title cell of the screen
            return detailTitleCell
            
        }
        // column data feature for iPad and iPhone
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailSystemHealthTableViewCell
            cell.titleLabel.text = "CALLS"
        
            cell.currentStatusView.backgroundColor = UIColor(hexString:"68BB45")
            cell.postStatusView.backgroundColor = UIColor(hexString:"C3D600")
            // System Health List cell
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        {
            // device is ipad
            if(indexPath.row == 0)
            {
                return 80
            }
            
            return 70
        }
            
        else if(indexPath.row == 0)
        {
            //device iPhone
          return 60
        }
        
        return 50
    }
    
}
