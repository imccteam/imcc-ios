//
//  IMCCContactSheetView.swift
//  IMCC
//
//  Created by FPL  on 17/01/17.
//  Copyright © 2017 FPL . All rights reserved.
//

import UIKit

class IMCCContactSheetView: UIView {
    
        @IBOutlet weak var btnFirstNumber: UIButton!
        @IBOutlet weak var btnSecondNumber: UIButton!
        @IBOutlet weak var btnCancel: UIButton!
        
        @IBOutlet weak var view: UIView!
        
    class func instanceFromNib() -> IMCCContactSheetView {
        return UINib(nibName: "IMCCContactSheetView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! IMCCContactSheetView
    }
}
