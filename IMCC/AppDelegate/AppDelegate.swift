//
//  AppDelegate.swift
//  IMCC
//
//  Created by FPL  on 29/11/16.
//  Copyright © 2016 FPL . All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    // MARK: Custom Methods
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // createMenuView
    
    fileprivate func createMenuView() {
        
        // create viewController code...
        var storyboard:UIStoryboard!
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad){
            // device is ipad
            storyboard = UIStoryboard(name: "Storyboard_iPad", bundle: nil)
        }
        else{
            //  device is iPhone
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }
        
        // Set Controllers in slider controller
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "IMCCHomeCon") as! IMCCHomeViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "IMCCLeftSliderCon") as! IMCCLeftSliderViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeViewController)
        leftViewController.homeViewController = nvc
        
        // SlideMenu Controller
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = homeViewController
        
        // Set Root view Controller
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
        
    // MARK: APP Delegates Methods
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // didFinishLaunchingWithOptions:
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.createMenuView()
        
        // Set Navigation Bar Color
        UINavigationBar.appearance().barTintColor = UIColor(red: 0, green: 150.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        // Return
        return true
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // applicationWillResignActive:
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // applicationDidEnterBackground:
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // applicationWillEnterForeground:
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // applicationDidBecomeActive:
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    // -------------------------------------------------------------------------------------------------------------------------------------------
    // applicationWillTerminate:
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

