//
//  UUBarChart.m
//  UUChartDemo
//
//  Created by shake on 14-7-24.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import "UUBarChart.h"
#import "UUChartLabel.h"
#import "UUBar.h"

@interface UUBarChart ()
{
    UIScrollView *myScrollView;
    UIView *contentView;
}
@end

@implementation UUBarChart {
    NSHashTable *_chartLabelsForX;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
    
        contentView = [[UIView alloc]initWithFrame:CGRectMake(UUYLabelwidth, 0, frame.size.width-UUYLabelwidth, frame.size.height)];

        
        // Axises
        CGSize contentViewSize = contentView.bounds.size;
        CGFloat borderWidth = 1.0;
        UIColor *borderColor = [UIColor darkGrayColor];
        UIView *left = [[UIView alloc]initWithFrame:CGRectMake(UUYLabelwidth + 10 / 5, - (UULabelHeight), borderWidth, contentViewSize.height - UULabelHeight)];
        UIView *bottom = [[UIView alloc]initWithFrame:CGRectMake(UUYLabelwidth + 10 / 5,contentViewSize.height - (UULabelHeight * 2),contentViewSize.width,1.0)];
        left.opaque = YES;
        bottom.opaque = YES;
        left.backgroundColor = borderColor;
        bottom.backgroundColor = borderColor;
        left.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
        bottom.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        [contentView addSubview:left];
        [contentView addSubview:bottom];
        
        
         [self addSubview:contentView];
//        myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(UUYLabelwidth, 0, frame.size.width-UUYLabelwidth, frame.size.height)];
//        [self addSubview:myScrollView];
    }
    return self;
}

-(void)setYValues:(NSArray *)yValues
{
    _yValues = yValues;
    [self setYLabels:yValues];
}

-(void)setYLabels:(NSArray *)yLabels
{
    NSInteger max = 0;
    NSInteger min = 1000000000;
    for (NSArray * ary in yLabels) {
        for (NSString *valueString in ary) {
            NSInteger value = [valueString integerValue];
            if (value > max) {
                max = value;
            }
            if (value < min) {
                min = value;
            }
        }
    }
    if (max < 5) {
        max = 5;
    }
    _yValueMin = 0;
    _yValueMax = (int)max;
    
    if (_chooseRange.max!=_chooseRange.min) {
        _yValueMax = _chooseRange.max;
        _yValueMin = _chooseRange.min;
    }

    float level = (_yValueMax-_yValueMin) /4.0;
    CGFloat chartCavanHeight = self.frame.size.height - UULabelHeight*3;
    CGFloat levelHeight = chartCavanHeight /4.0;
    
    for (int i=0; i<5; i++) {
        UUChartLabel * label = [[UUChartLabel alloc] initWithFrame:CGRectMake(0,chartCavanHeight-i*levelHeight+5, UUYLabelwidth + 10, UULabelHeight)];
        
        CGFloat borderWidth = 4.0;
        UIColor *borderColor = [UIColor darkGrayColor];
        UIView *left = [[UIView alloc]initWithFrame:CGRectMake(label.frame.size.width + 8,label.frame.origin.y, borderWidth, 1.0)];
        label.textAlignment = NSTextAlignmentRight;
         left.opaque = YES;
        left.backgroundColor = borderColor;
        [label addSubview:left];
        
        NSLog(@"%f",level * i + _yValueMin);
		label.text = [NSString stringWithFormat:@"%.0f",level * i+_yValueMin];
		[self addSubview:label];
    }
}

-(void)setXLabels:(NSArray *)xLabels
{
    _currentOrientation = [[UIDevice currentDevice]orientation];
    CGFloat customFontSize;
    
    if( !_chartLabelsForX ){
        _chartLabelsForX = [NSHashTable weakObjectsHashTable];
    }
    
    _xLabels = xLabels;
    NSInteger num;
    int xPos;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        customFontSize = 13.0;
        switch (_currentOrientation) {
            case UIDeviceOrientationLandscapeRight:
                xPos = 60;
                break;
            case UIDeviceOrientationLandscapeLeft:
                xPos = 55;
                break;
            default:
                xPos = 55;
                break;
        }
    }
    else{
          customFontSize = 10.0;
        switch (_currentOrientation) {
            case UIDeviceOrientationLandscapeRight:
                xPos = 45;
                break;
            case UIDeviceOrientationLandscapeLeft:
                xPos = 45;
                break;
            default:
                xPos = 35;
                break;
        }
    }
//    if (xLabels.count>=8) {
//        num = 8;
//    }else if (xLabels.count<=4){
//        num = 4;
//    }else{
        num = xLabels.count;
    //}
    //_xLabelWidth = myScrollView.frame.size.width/num; 40  45
   
    _xLabelWidth = contentView.frame.size.width/num;
     NSLog(@"%f",_xLabelWidth);
    
    for (int i=0; i<xLabels.count; i++) {
        UUChartLabel * label = [[UUChartLabel alloc] initWithFrame:CGRectMake((i *  _xLabelWidth + xPos), self.frame.size.height - UULabelHeight, _xLabelWidth / 2, UULabelHeight)];
        label.text = xLabels[i];
        label.backgroundColor = [UIColor orangeColor];
        
        label.font = [UIFont fontWithName:@"Helvetica" size:customFontSize]; //[UIFont systemFontOfSize:12.0];
        label.textAlignment = NSTextAlignmentCenter;
        [contentView addSubview:label];
        //[myScrollView addSubview:label];
        
        [_chartLabelsForX addObject:label];
    }
    
    //float max = (([xLabels count]-1)*_xLabelWidth + chartMargin)+_xLabelWidth;
//    if (myScrollView.frame.size.width < max-10) {
//        myScrollView.contentSize = CGSizeMake(max, self.frame.size.height);
//    }
//        if (contentView.frame.size.width < max-10) {
//            c
//            contentView.contentMode = CGSizeMake(max, self.frame.size.height);
//        }

}

-(void)setColors:(NSArray *)colors
{
	_colors = colors;
}

- (void)setChooseRange:(CGRange)chooseRange
{
    _chooseRange = chooseRange;
}

-(void)strokeChart
{
    _currentOrientation = [[UIDevice currentDevice]orientation];
    CGFloat intervalSpace;
    CGFloat barWidth;
    UUBar *bar = [[UUBar alloc]init];
    CGFloat chartCavanHeight = self.frame.size.height - UULabelHeight*3;
	
    for (int i=0; i<_yValues.count; i++) {
//        if (i==2)   width:  _xLabelWidth * (_yValues.count==1?0.8:0.45) -
        // (j+(_yValues.count==1?0.1:0.05))*_xLabelWidth +i*_xLabelWidth * 0.47
//            return;
        NSArray *childAry = _yValues[i];
        for (int j=0; j<childAry.count; j++) {
            NSString *valueString = childAry[j];
            float value = [valueString floatValue];
            float grade = ((float)value-_yValueMin) / ((float)_yValueMax-_yValueMin);
            
            //MARK: Width Specific to orientation
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                switch (_currentOrientation) {
                    case UIDeviceOrientationLandscapeRight:
                        intervalSpace = 0.15;
                        barWidth = 35;
                        break;
                        
                    default:
                        intervalSpace = 0.2;
                        barWidth = 35;
                        break;
                }
                bar = [[UUBar alloc] initWithFrame:CGRectMake((j+(_yValues.count==1?0.1:0.25))*_xLabelWidth + (i *_xLabelWidth * intervalSpace), UULabelHeight,barWidth , chartCavanHeight)];
            }
            else
            {
                switch (_currentOrientation) {
                    case UIDeviceOrientationLandscapeRight:
                        intervalSpace = 0.1;
                        barWidth = 19.33;
                        break;
                        
                    default:
                        intervalSpace = 0.19;
                        barWidth = 19.33;
                        break;
                }
                bar = [[UUBar alloc] initWithFrame:CGRectMake((j+(_yValues.count==1?0.1:0.3))*_xLabelWidth + (i *_xLabelWidth * intervalSpace), UULabelHeight,barWidth , chartCavanHeight)];
                
            }
            
            
            //Ipad Cal
//            UUBar * bar = [[UUBar alloc] initWithFrame:CGRectMake((j+(_yValues.count==1?0.1:0.3))*_xLabelWidth + (i *_xLabelWidth * 0.25), UULabelHeight,25, chartCavanHeight)];
            
            
            //MARK: BAR TEXT
            int labelYValue;
             UIFont *TextFont = [UIFont fontWithName:@"Helvetica" size:9.0];
            NSString *labelText	= [NSString stringWithFormat:@"%@",valueString];
             CGSize barTextsize = [labelText sizeWithAttributes:@{NSFontAttributeName:TextFont}];
            NSLog(@"%f", barTextsize.width);
            NSLog(@"%f",barTextsize.height);
            CGRect barTextFrame		= CGRectMake(0.0,
                                                 0.0,
                                                 barTextsize.width,
                                                 barTextsize.height);
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                labelYValue = (1 - grade) * bar.frame.size.height - 8;
            }
            else
            {
                labelYValue = (1 - grade) * bar.frame.size.height - 3;
            }
            UILabel *label = [[UILabel alloc]initWithFrame:barTextFrame];
            label.center = CGPointMake(bar.frame.size.width/2.0 ,labelYValue);
//            label.center = CGPointMake(bar.frame.size.width/2.0 + bar.frame.origin.x ,labelYValue);
            label.font = TextFont;
            label.text = labelText;
            label.textAlignment = NSTextAlignmentCenter;
            label.numberOfLines = 0;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            [bar addSubview:label];
            NSLog(@"%f", bar.frame.origin.x);
            NSLog(@"%f", bar.frame.origin.y);
            NSLog(@"%f", bar.frame.size.width);
            NSLog(@"%f", bar.frame.size.height);
            
            bar.barColor = [_colors objectAtIndex:i];
            NSLog(@"%f",grade);
            bar.gradePercent = grade;
            [contentView addSubview:bar];
            //[myScrollView addSubview:bar];
            
        }
    }
}

- (NSArray *)chartLabelsForX
{
    return [_chartLabelsForX allObjects];
}

@end
